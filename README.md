# BL Starter
Just a [Blendid](https://github.com/vigetlabs/gulp-starter/tree/blendid) (aka gulp-starter) fork.

## Requirements
1. Node 6+ ([NVM](https://github.com/creationix/nvm) reccomended)
2. [Yarn](https://yarnpkg.com/en/docs/install#alternatives-tab)

## All you need to know
Source files are located in the `src` directory.

**Add dependencies** - Take a look at [Yarn docs](https://yarnpkg.com/en/docs/cli/add)
```bash
yarn add [package-name]
```
**Install dependencies**
```bash
yarn
```
**Watch source files**
```bash
gulp
```
**Build production files**
```bash
gulp build
```
[_TODO_] **Deploy production files**
```bash
gulp deploy
```

### Notes
[_WIP_] `README` files in `src` directory need to be updated.

### Credits
[Viget](https://github.com/vigetlabs)
